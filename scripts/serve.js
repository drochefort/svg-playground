// require the module as normal
const browserSync = require('browser-sync');

// Start the server
browserSync({
    server: ['src','.'],
    files: [
        '*.html',
        'js/*.js',
        'css/*.css',
        'svg/*.svg',
    ],
});