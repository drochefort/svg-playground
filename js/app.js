(function () {
    const illustration = document.getElementById('illustration');
    const svgFile = 'svg/backpack.svg';

    function load() {
        return fetch(svgFile)
            .then(response => response.text())
            .then(data => illustration.innerHTML = data);
    }

    function setStep(className) {
        illustration.className = '';
        illustration.classList.add(className);
    }

    function decorateSVG() {
        return fetch(svgFile.replace('.svg', '.json'))
            .then(response => response.json())
            .then((data) => data.frames.forEach(frame => frame.items.forEach(item => {
                const el = document.getElementById(item);
                el.classList.add(frame.name);
            })));
    }

    function attachEvents() {
        document.getElementById('button-1').addEventListener('click', () => setStep('step-1'));
        document.getElementById('button-2').addEventListener('click', () => setStep('step-2'));
        document.getElementById('button-3').addEventListener('click', () => setStep('step-3'));
        document.getElementById('button-4').addEventListener('click', () => setStep('step-4'));

        return Promise.resolve();
    }

    function initialPlayback() {
        setTimeout(() => setStep('step-2'), 500);
        setTimeout(() => setStep('step-4'), 600);
        setTimeout(() => setStep('step-3'), 700);
        setTimeout(() => setStep('step-1'), 800);
    }

    load()
        .then(decorateSVG)
        .then(attachEvents)
        .then(initialPlayback);
})();