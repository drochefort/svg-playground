# Paced SVG POC

This is a playground to test possible solutions for authoring paced animations for MX RTI.

## Suggested Authoring Flow

1. Illustration is designed in Adobe Illustrator then exported to SVG following these rules:
    - Each layer has a name, that becomes an ID in the exported SVG. Illustrator does that automatically.
    - Each step can be associated with a subset of layers in the illustration.
    - an example of such a file: svg/backpack.svg

2. A JSON file is provided along with the illustration, mapping each of the animation step with the associated subset of layers.
    - an example of such a file: svg/backpack.json

3. Then within Habitat, LA will simply have to associate the appropriate SVG **step** with the corresponding interaction **step**. 

## Unknowns

Not sure how to author animated transitions between steps, or sequences.
